console.log('index.js');

/* functions */
	function calculateSubject(code) {
		// convert each array element into ASCII character const subjectArray = XXX
		// add your code here
		const subjectArray = code.map(number => String.fromCharCode(number));

		// j:D - get Subject id to displays it in input
		document.getElementById('subject').value = `Zu handen Julia, ${subjectArray.join('')}`;
		
		// create string from array
		return `Zu handen Julia, ${subjectArray.join('')}`;
	};

	function calculateCode(token) {
		// use characters ASCII decimal value
		const codeArray = Array
									.from(token)
									.map(char => char.charCodeAt(0) === 32
										? 32
										: char.charCodeAt(0) % 65 + 63);

		return codeArray;
	};

	function calculateReceiver(token) {
		let sum = 0;
		// sum up ASCII decimal values of all characters in the token
		// store result in sum
		// add your code here
		sum = Array
			.from(token)
			.reduce(function(acc, val) {
				return acc + val.charCodeAt(0);
			}, 0);

		// j:D - get Subject Id again to add receiver
		document.getElementById('subject').value += ` <jobs${sum%600}@eguana.at>`;

		return `jobs${sum%600}@eguana.at`;
	};

	function writeYourOwnLetterOfApplication() {
		let divNode = document.createElement('div');
		let emailIngredient = [
			document.createTextNode('Sehr geehrte Fr. Julia Stefaner,'),
			document.createElement('br'),
			document.createTextNode('sehr geehrtes Development Team,'),
			document.createElement('br'),
			document.createElement('br'),
			document.createTextNode('anbei erhalten Sie meine Bewerbung für Ihre ausgeschriebene Stelle als Frontend Unterstützer. Warum ich die Stelle optimal ausfüllen kann und Ihrem Unternehmen durch meine Erfahrung und Können zahlreiche Vorteile biete, entnehmen Sie bitte meinen ausführlichen und angehängten Bewerbungsunterlagen.'),
			document.createElement('br'),
			document.createTextNode('Ich freue mich auf ein persönliches Vorstellungsgespräch.'),
			document.createElement('br'),
			document.createElement('br'),
			document.createTextNode('Mit besten Grüßen'),
			document.createElement('br'),
			document.createTextNode('Johndrick Calupas')
		]
		emailIngredient.forEach((item, index, arr) => divNode.appendChild(item));
		// document.body.appendChild(divNode);

		// j:D -	replaces br with \n for new line
		// 		to display it in textarea
		const emailPlainText = divNode.innerHTML.replace(/<br>/g, '\n');
		document.getElementById('email').value = emailPlainText;

		return divNode.innerHTML;
	};

	function sendEmail({receiver, subject, attachment, text}) {
		console.log(`receiver: ${receiver},\nsubject: ${subject},\nattachment: ${attachment},\ntext: ${text}`);
		// TODO
	}

	function buildEmail() {
		// read value from input
		// use this token: fgt Dguvgp
		// const token = 'fgt Dguvgp';
		const token = document.getElementById('token').value;
		const emailAttachment = document.getElementById('attachment').href;
		const letterOfApplication = writeYourOwnLetterOfApplication();
		const subject = calculateSubject(calculateCode(token));
		const receiver = calculateReceiver(token);

		sendEmail({
			receiver,
			subject,
			attachment: emailAttachment,
			text: letterOfApplication
		});
	};


$(document).ready(function() {
/* j:D - Testing purpose
	writeYourOwnLetterOfApplication();
	calculateSubject(calculateCode('fgt Dguvgp'));
	calculateReceiver('fgt Dguvgp'); */

	// buildEmail();
});